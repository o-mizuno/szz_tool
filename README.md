# SZZ Tool #

This is an implementation of the SZZ algorithm.

## Need to prepare ##

* A csv file that have "issue id, created date, closed date"
* A repository of the target software.

What's new
---------------
2014-11-06 created date and closed date now need time information. (in the format of YYYY-MM-DD hh:mm:ss)