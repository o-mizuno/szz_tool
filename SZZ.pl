#!/usr/bin/perl

use strict;
use utf8;
use Getopt::Long;
use Data::Dumper;
use FindBin qw($Bin);
use lib "$Bin/lib";

use GitTagger;

sub usage {
    print <<EOM;
Usage: SZZ.pl --csv-dir     /path/to/dir/contains/csv [required]
              --target-dir  /path/to/git/repogitory   [required]
              --historage   turn on if using historage (false)
              --step        [1|2|3] (default: 1 and 2)

How to use:
 The issue id and opened and closed dates for SZZ algorithm are specified 
 in csv files in the [csv-dir].
 [target-dir] specifies the git repository targetted.
 [step] specifies 1. identifying fixed points 2. identifying bug injected points, 
 and 3. identifying faulty and innocent modules. 

 By default, SZZ.pl only does steps 1 and 2. It is recommended that you back up the 
 repository after step 2 since step 2 and 3 takes a looooong time.

Data:
 You can use multiple csv files for this SZZ tool.
 Put all csv files in the [csv-dir].

 The 1st row of a csv file is a header row and discarded in the SZZ tool. 
 A csv file should be formatted from 3 columns:
 bug issue-id (numeric),open date (YYYY-MM-DD),closed or fixed date (YYYY-MM-DD)

 ex:
 issue,opened,closed
 1,2001-07-30 05:00:00,2001-12-24 06:30:12
 2,2002-01-08 06:00:00,2003-02-02 23:00:00
 ...

EOM
    exit 0;
}

my $CSV_DIR;
my $TARGET;
my @STEP;
my $HELP;
my $HISTORAGE = 0;
my $QUIET = 0;

GetOptions('csv-dir=s' => \$CSV_DIR,
	   'target-dir=s' => \$TARGET, 
	   'historage' => \$HISTORAGE,
	   'quiet' => \$QUIET,
	   'help'  => \$HELP,
	   'step=i'  => \@STEP,
    );

&usage() if (!$CSV_DIR);
&usage() if (!$TARGET);
&usage() if ($HELP);

system "git --git-dir=$TARGET/.git branch | grep \\* | tr -d '\\* ' | tr -d '\\n' > $TARGET/now_head";
system "git --git-dir=$TARGET/.git log --reverse --pretty=\%H | head -1 | tr -d '\\n'  > $TARGET/init_com";

my $gittagger = new GitTagger(
    git_path => $TARGET, 
    bug_csv_dir => $CSV_DIR, 
    tag_ini =>  'FIX',
    set_ini =>  'BUG',   
    fault_ini =>  'FAULT',
    innocent_ini =>  'INNOCENT',   
    historage =>  $HISTORAGE,   
    now_head => `cat $TARGET/now_head`,
    init_com => `cat $TARGET/init_com`
    );

unlink("$TARGET/now_head","$TARGET/init_com");

if (@STEP == ()) {
    push(@STEP,1);
    push(@STEP,2);
}

@STEP = sort(@STEP);

foreach (@STEP) {
    if ($_ == 1) {
	print STDERR "[01] set FIX tags...\n";
	#system "./setFIXtags.pl $TARGET $CSV_DIR 'FIX'";
	$gittagger->setFIXtag();
    } elsif ($_ == 2) {
	print STDERR "[02] set BUG tags...\n";
	$gittagger->setBUGtag();
        # system "./setBUGtags_simple.pl $TARGET $CSV_DIR 'FIX' 'BUG' \"$now_head\" $init_com";
    } elsif ($_ == 3) {
	print STDERR "[03] set FAULT/INNOCENT tags...\n";
	$gittagger->setFAULTtag();
    }
}

print STDERR "done.\n";
exit 0;


