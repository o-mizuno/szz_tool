#!/usr/bin/perl

use 5.008;
use strict;

our (@ISA, @EXPORT);

require Exporter;

@ISA = qw(Exporter);

our @EXPORT = qw(escape git_checkout git_dtag gt_ftag git_tag_obj git_gc);

use Carp qw(carp croak);


sub escape {
  my $path = shift;

  $path =~ s|\(|\\(|g;
  $path =~ s|\)|\\)|g;
  $path =~ s|\<|\\<|g;
  $path =~ s|\>|\\>|g;
  $path =~ s|(\s)|\\$1|g;
  $path =~ s|(\$)|\\$1|g;

  return $path;
}

sub git_checkout {
  my ($git_path, $obj) = @_;

  my $gc_status = system("git", "--git-dir=$git_path/.git", "--work-tree=$git_path", "checkout", "-q", "-f", $obj);
  die "git checkout exited funny: $?" unless $gc_status == 0;

  return 0;
}

sub git_dtag {
  my ($git_path, $tag) = @_;

  my $gc_status = system("git", "--git-dir=$git_path/.git", "--work-tree=$git_path", "tag",
			 "-d", $tag);
  die "git delete tag exited funny: $?" unless $gc_status == 0;

  return 0;
}

sub git_ftag {
  my ($git_path, $tag, $msg) = @_;

  if (length($msg) < 4096) {
      my $gc_status = system("git", "--git-dir=$git_path/.git", 
			     "--work-tree=$git_path", "tag",
			     "-f", "-a", $tag, "-m", $msg);
      die "git force tag exited funny: $? : tag -f -a $tag -m $msg" unless $gc_status == 0;
  } else {
      open(GIT,"| git --git-dir=$git_path/.git --work-tree=$git_path tag -f -a $tag -F -") || die "git force tag exited funny: $? : tag -f -a $tag -F -";
      print GIT $msg;
      close(GIT);
  }
  return 0;
}

sub git_tag_obj {
  my ($git_path, $tag, $msg, $obj) = @_;
  
  my $gc_status = system("git", "--git-dir=$git_path/.git", "--work-tree=$git_path", "tag",
			 "-a", $tag, "-m", $msg, $obj);
  die "git tag obj exited funny: $? : tag -a $tag -m $msg $obj" unless $gc_status == 0;

  return 0;
}

sub git_gc {
  my $git_path = shift;

  my $gc_status = system("git", "--git-dir=$git_path/.git", "--work-tree=$git_path", "gc");
  die "git gc exited funny: $?" unless $gc_status == 0;

  return 0;
}


1;
