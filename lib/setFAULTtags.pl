#!/usr/bin/perl

use strict;
use Getopt::Long;
use Data::Dumper;

sub usage {
    print <<EOM;
usage: getFaultyModules_java.pl --target-dir /path/to/repo
                                --module-dir /path/to/modules
EOM
    exit 0;
}

# コマンドラインオプション，設定ファイル読み込み

my $TARGET;
my $MODULE;
my $HELP;
my $QUIET;
my $FILE  = 1;
my $TAG   = 0;

GetOptions('target-dir=s' => \$TARGET, 
	   'module-dir=s' => \$MODULE,
	   'tag!'  => \$TAG,
	   'file!' => \$FILE,
	   'quiet' => \$QUIET,
	   'help'  => \$HELP
    );

&usage() if (!$TARGET);
&usage() if ($FILE && !$MODULE);
&usage() if ($HELP);

my $dir_f = "$MODULE/f";
my $dir_n = "$MODULE/n";
if ($FILE) {
    if (-d $dir_f) {
	system("rm -rf $dir_f");
    }
    if (-d $dir_n) {
	system("rm -rf $dir_n");
    }
    mkdir $dir_f;
    mkdir $dir_n;
}

print STDERR "[01 Getting bug tags]\n" if (!$QUIET);
my @list_BUGtag = split(/\n/,`git --git-dir=$TARGET/.git tag | grep BUG`);
#my @list_FIXtag = split(/\n/,`git --git-dir=$TARGET/.git tag | grep FIX`);

my %found;

print STDERR "[02 Identifying faulty modules]\n" if (!$QUIET);
foreach my $bugtag (sort(@list_BUGtag)) {
    $bugtag =~/^BUG-(\d+)-(\d+)-(\d+)$/;
    my $issue = $1;
    my $fixid = $2;
    my $bugid = $3;
    my $fixtag = "FIX-$issue-$fixid";
    my @javabugfiles;

    # get buggy files at the point of bugtag
    my @bugfiles = split(/@@/,`git --git-dir=$TARGET/.git show --pretty=short $bugtag| head -4 | tail -1`);
    foreach my $f (@bugfiles) {
	next if ($f =~/^\s*$/ || $f !~/\.java$/);
	push(@javabugfiles,$f);
    }

    # List commits between bugtag and fixtag
    my @commits = split(/\n/, `git --git-dir=$TARGET/.git log $bugtag..$fixtag | grep "^commit " | cut -d ' ' -f 2`);
    my $x = 0;
    for (my $i = 0; $i<=$#commits;$i++) {
	my $c = $commits[$i];
	for (my $j = 0; $j <= $#javabugfiles; $j++){
	    my $jf = $javabugfiles[$j];
	    my $gitshowfile = `git --git-dir=$TARGET/.git show $c -- '$jf' | grep "^index "`;
	    next if ($gitshowfile eq "");
	    my ($dummy1,$hashes) = split(/\s+/,$gitshowfile);
	    my $blobhash;
	    my $dummy2;
	    if ($i == 0) {
		($dummy2,$blobhash) = split(/\.\./,$hashes);
	    } else {
		($blobhash,$dummy2) = split(/\.\./,$hashes);
	    }
	    chomp($blobhash);
	    next if ($blobhash == 0);
	    next if ($found{$blobhash});

	    $found{$blobhash} = 1;
	    printf STDERR "[T:%20s C:%4d/%4d F:%4d/%4d] $blobhash\n",$bugtag,$i,$#commits,$j,$#javabugfiles if (!$QUIET);
	    if ($TAG) {
		system "git --git-dir=$TARGET/.git tag FAULT-$issue-$fixid-$bugid-$x $blobhash";
		$x++;
	    }
	    if ($FILE) {
		system "git --git-dir=$TARGET/.git show $blobhash > $dir_f/$blobhash.java";
	    }
	}
    }
}

print STDERR "[03 Identifying non-faulty modules]\n" if (!$QUIET);
my @faulthash = keys %found;
my @allcommits = split(/\n/, `git --git-dir=$TARGET/.git log | grep "^commit "| cut -d ' ' -f 2`);

my $x = 0;
for (my $i = 0; $i<=$#allcommits;$i++) {
    my $c = $allcommits[$i];
    my @files = `git --git-dir=$TARGET/.git show $c | grep "^index "`;
    next if (@files == ());
    for (my $j = 0; $j <= $#files; $j++) {
	my ($dummy1,$hashes) = split(/\s+/,$files[$j]);
	my $blobhash;
	my $dummy2;
	($dummy2,$blobhash) = split(/\.\./,$hashes);
	chomp($blobhash);
	next if ($blobhash == 0);
	next if ($found{$blobhash});
	$found{$blobhash} = 2;
	printf STDERR "[C:%4d/%4d F:%4d/%4d] $blobhash\n",$i,$#allcommits,$j,$#files if (!$QUIET);
	if ($TAG) {
	    system "git --git-dir=$TARGET/.git tag INNOCENT-$x $blobhash";
	    $x++;
	}
	if ($FILE) {
	    system "git --git-dir=$TARGET/.git show $blobhash > $dir_n/$blobhash.java";
	}
    }
}
