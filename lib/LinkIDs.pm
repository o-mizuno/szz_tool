#!/usr/bin/perl

use 5.008;
use strict;

our (@ISA, @EXPORT, %EXPORT_TAGS);

require Exporter;

@ISA = qw(Exporter);

our @EXPORT = qw(get_ids git_tag_obj git_gc);
our @EXPORT_OK = qw(szz_diff_simple szz_diff_mt szz_blame_simple szz_blame_mt);

%EXPORT_TAGS = (
		fix_tags        => [qw(get_ids examine_msg__set_ftag git_gc)],
		bug_tags_simple => [qw(get_ids szz_diff_simple szz_ftag szz_blame_simple szz_btag
				       git_checkout git_dtag git_ftag git_tag_obj git_gc)],
		bug_tags_mt     => [qw(get_ids szz_diff_mt szz_ftag szz_blame_mt szz_btag
				       git_checkout git_dtag git_ftag git_tag_obj git_gc)],
	       );

use Carp qw(carp croak);

use LinkIDs::2git;


sub get_ids {
  my ($ref_ids, $bug_csv_dir) = @_;
  my %jira_prjs = ();

  opendir my $dh, $bug_csv_dir
    or die "Couldn't open $bug_csv_dir: $!";
  while (defined (my $bug_csv = readdir($dh))) {
    next unless ($bug_csv =~ /\.csv$/);
    print "$bug_csv\n";

    my $prj = '';
    open my $list, "<", $bug_csv_dir . '/' . $bug_csv
      or die "Couldn't open $bug_csv: $!";
    while (my $id = <$list>) {
      $id =~ s/"//g; # bugzilla
      $id =~ s/\//-/g; # jira
#      $id =~ s/ [\d:]+//g; #bugzilla, jira

      if ($id =~ /([^,]+),([\d\-\/:\s]+),([\d\-\/:\s]+)/) {
	my $bid = $1;
	my $odate = $2;
	my $cdate = $3;

	print STDERR $bid,":",$odate,":",$cdate,"\n";

	$ref_ids->{$bid}{open} = $odate;
	$ref_ids->{$bid}{chgd} = $cdate;

	if (not $prj and $bid =~ /([A-Z]+)-\d+/) {
	  $prj = $1;
	  $jira_prjs{$prj} = 1;
	}
      }
    }
    close $list
      or die "Couldn't close a list file: $!";

  }
  closedir $dh;

  return \%jira_prjs;
}

sub examine_msg__set_ftag {
  my ($git_path, $tag_ini, $com_date, $sha, $msg, $ref_id2date, $count, $prj) = @_;
  my %checked_id = ();

  while ($msg =~ /(\d+)/g) {
    my $num = $1;
    my $id = $prj ? $prj . '-' . $num : $num;

    next if (exists $checked_id{$id});
    $checked_id{$id} = 1;
    next unless ($msg =~ /$id/);

    if (exists $ref_id2date->{$id}) {
      my ($yyyy, $mm, $dd, $HH, $MM, $SS) = ($ref_id2date->{$id}{open} =~ /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
      my $ep_id_open = timelocal($SS, $MM, $HH, $dd, $mm-1, $yyyy-1900);
      ($yyyy, $mm, $dd,$HH, $MM, $SS) = ($ref_id2date->{$id}{chgd} =~  /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
      my $ep_id_chgd = timelocal($SS, $MM, $HH, $dd, $mm-1, $yyyy-1900);
      ($yyyy, $mm, $dd,$HH, $MM, $SS) = ($com_date =~  /(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);
      my $ep_com_date = timelocal($SS, $MM, $HH, $dd, $mm-1, $yyyy-1900);

      if ($ep_com_date >= $ep_id_open and $ep_com_date <= $ep_id_chgd) {
	my $tag = $tag_ini . '-' . $id . '-' . $count;
	git_tag_obj($git_path, $tag, 'with Bugzilla', $sha);
	print " [Tagged ($tag)]\n";
	$count++;
      }
    }
  }

  return $count;
}

sub szz_diff_simple {
  my ($ref_data, $git_path, @files) = @_;
  my $path = escape($files[0]);
  $path .= ' ' . escape($files[1]) if ($files[1]);

  my $git_difftool_option = '-M -l 3000 --no-prompt --extcmd=diff -b -w HEAD^ HEAD';
  open my $git_difftool, "-|", "git --git-dir=$git_path/.git --work-tree=$git_path difftool"
    . " $git_difftool_option -- $path"
    or die "Couldn't open git difftool: $!";
  while (my $dline = <$git_difftool>) {
    #next if ($dline =~ /^</ or $dline =~ /^>/ or $dline =~ /^---/ or $dline =~ /a/);
    next unless ($dline =~ /^([\d,]+)[cd]/);
    my $pre_line = $1;
    $pre_line .= ','.$pre_line if ($pre_line !~ /,/); # for one line

    $ref_data->{$files[0]}{$pre_line} = 1;
  }
  close $git_difftool
    or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git difftool";

  return 0;
}

sub szz_diff_mt {
  my ($ref_data, $ref_java, $git_path, @files) = @_;
  my $path = escape($files[0]);
  $path .= ' ' . escape($files[1]) if ($files[1]);

  my $git_difftool_option = '-M -l 3000 --no-prompt --extcmd=diff -b -w HEAD^ HEAD';
  open my $git_difftool, "-|", "git --git-dir=$git_path/.git --work-tree=$git_path difftool"
    . " $git_difftool_option -- $path"
    or die "Couldn't open git difftool: $!";
  while (my $dline = <$git_difftool>) {
    next if ($dline =~ /^</ or $dline =~ /^>/ or $dline =~ /^---/ or $dline =~ /a/);
    $dline =~ /^([\d,]+)[cd]/;
    my $pre_line = $1;
    $pre_line .= ','.$pre_line if ($pre_line !~ /,/); # for one line

    $ref_data->{$files[0]}{$pre_line} = 1;

    if ($files[0] =~ /\/MT\//) {
      my $java = $files[0];
      $java =~ s|\.f/CL/.*?$|\.java|;
      $ref_java->{$java} = 1;
    }
  }
  close $git_difftool
    or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git difftool";

  return 0;
}

sub szz_ftag {
  my ($ref_list, $git_path, $tag) = @_;

  my $list = '@@';
  for my $file (keys %$ref_list) {
    $list .= $file.'@@';
  }
  $list .= '@@';

  git_ftag($git_path, $tag, $list);

  return 0;
}

sub szz_blame_simple {
  my ($ref_data, $git_path) = @_;
  my %checked_com = ();

  for my $file (keys %$ref_data) {
    for my $nums (keys %{ $ref_data->{$file} }) {

      my $git_blame_option = "--incremental -L $nums -w";
      my ($bcom, $ep_date, $ori_file) = ();
      open my $git_blame, "-|", "git --git-dir=$git_path/.git --work-tree=$git_path blame"
	. " $git_blame_option -- " . escape($file)
	  or die "Couldn't open git blame: $!";
      while (my $line = <$git_blame>) {
	if ($line =~ /^(\w{40})/) {
	  $bcom = $1;
	}
	elsif ($line =~ /^committer-time (\d+)\n$/) {
	  $ep_date = $1;
	}
	elsif ($line =~ /^filename (.*)\n$/) {
	  $ori_file = $1;

	  $checked_com{$bcom}{date} = $ep_date;
	  $checked_com{$bcom}{file}{$ori_file} = 1;
	}
      }
      close $git_blame
	or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git blame ($file)";

    }
    print '.';
  }
  print "\n";

  return \%checked_com;
}

sub szz_blame_mt {
  my ($ref_data, $ref_java, $git_path) = @_;
  my %checked_com = ();

  for my $file (keys %$ref_data) {
    for my $nums (keys %{ $ref_data->{$file} }) {

      my $git_blame_option = "--incremental -L $nums -w";
      my ($bcom, $ep_date, $ori_file) = ();
      open my $git_blame, "-|", "git --git-dir=$git_path/.git --work-tree=$git_path blame"
	. " $git_blame_option -- " . escape($file)
	  or die "Couldn't open git blame: $!";
      while (my $line = <$git_blame>) {
	if ($line =~ /^(\w{40})/) {
	  $bcom = $1;
	}
	elsif ($line =~ /^committer-time (\d+)\n$/) {
	  $ep_date = $1;
	}
	elsif ($line =~ /^filename (.*)\n$/) {
	  $ori_file = $1;

	  $checked_com{$bcom}{date} = $ep_date;
	  $checked_com{$bcom}{file}{$ori_file} = 1;

	  if ($ori_file =~ /\/MT\//) {
	    my $java = $ori_file;
	    $java =~ s|\.f/CL/.*?$|\.java|;
	    $ref_java->{$java} = 1;
	  }
	}
      }
      close $git_blame
	or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git blame";

    }
    print '.';
  }
  print "\n";

  return \%checked_com;
}

sub szz_btag {
  my ($ref_list, $git_path, $btag, $com) = @_;

  my $list = '@@';
  for my $file (keys %$ref_list) {
    $list .= $file.'@@';
  }
  $list .= '@@';

  git_tag_obj($git_path, $btag, $list, $com);

  return 0;
}


1;
