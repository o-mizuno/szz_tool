#!/usr/bin/perl

use strict;
use warnings;

package GitTagger;
use Time::Local qw(timelocal);

use FindBin qw($Bin);
use lib "$Bin/lib";
use LinkIDs qw(:fix_tags :bug_tags_simple);

my %id2date = ();
my $ref_jira_prjs;

sub new {
  my $class = shift;
  my $self = {
      git_path       => '.',
      bug_csv_dir    => '.',
      tag_ini        => 'FIX',
      set_ini        => 'BUG',
      fault_ini      => 'FAULT',
      innocent_ini   => 'INNOCENT',
      now_head        => '0',
      init_com       => '0',
      histrage       => 0,
      parallel       => 0,
      @_,
  };

  $ref_jira_prjs = get_ids(\%id2date, $self->{bug_csv_dir});

  return bless $self, $class;
}



sub setFIXtag {
    my ($self) = @_;


    my $git_path = $self->{git_path};
    my $tag_ini = $self->{tag_ini};
    
#    my %id2date = ();
#    my $ref_jira_prjs = get_ids(\%id2date, $bug_csv_dir);

    print "--->SZZ step1: linking\n";

# bugzilla
    my $count = 1;
    my $git_log_option = '--pretty="%cd@@%H@@%s" --date=iso --reverse';
    #. ' --grep=bug --grep=Bug --grep=BUG'
    #. ' --grep=fix --grep=Fix --grep=FIX';
    open my $git_log, "-|", "git --git-dir=$git_path/.git log $git_log_option"
	or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git log";
    while (my $line = <$git_log>) {
	$line =~ /([\d\-]+)\s([\d:]+)\s[\d\+\-]+@@(\w+)@@(.*?)\n/;
	my $com_date = $1." ".$2;
	my $sha = $3;
	my $msg = $4;
	
	$count = examine_msg__set_ftag($git_path, $tag_ini, $com_date, $sha, $msg, \%id2date, $count, '');
    }
    print "\n";
    close $git_log
	or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git log";


#jira
    for my $prj (keys %$ref_jira_prjs) {
	my $count = 1;
	my $git_log_option2 = '--pretty="%cd@@%H@@%s" --date=short --reverse'
	    . ' --grep=' . $prj;
	open my $git_log2, "-|", "git --git-dir=$git_path/.git log $git_log_option2"
	    or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git log";
	while (my $line = <$git_log2>) {
	    $line =~ /([\d\-]+)@@(\w+)@@(.*?)\n/;
	    my $com_date = $1;
	    my $sha = $2;
	    my $msg = $3;
	    
	    $count = examine_msg__set_ftag($git_path, $tag_ini, $com_date, $sha, $msg, \%id2date, $count, $prj);
	}
	print "\n";
	close $git_log2
	    or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git log";
    }
    
    git_gc($git_path);
}

sub setBUGtag {
    my ($self) = @_;

#   use LinkIDs qw(:bug_tags_simple);
    my $git_path = $self->{git_path};
    my $tag_ini = $self->{tag_ini};
    my $set_ini = $self->{set_ini};
    my $now_head = $self->{now_head};
    my $init_com = $self->{init_com};;

    my $git_tag_option = "-l $tag_ini-*";
    my %fix_com = ();

    print '--->Get tags';
    open my $git_tag, "-|", "git --git-dir=$git_path/.git tag $git_tag_option"
	or die "Couldn't open git tag: $!";
    while (my $fix_tag = <$git_tag>) {
	chomp $fix_tag;
	
	my $com_hash = '';
	my $git_log_option = '-1 --pretty=%H';
	open my $git_log, "-|", "git --git-dir=$git_path/.git log $git_log_option $fix_tag"
	    or die "Couldn't open git log: $!";
	while (my $line = <$git_log>) {
	    chomp $line;
	    $com_hash = $line;
	}
	close $git_log
	    or warn $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git log";

	if ($com_hash ne $init_com) {
	    $fix_tag =~ /$tag_ini-(.*)/;
	    my $tag_tail = $1;
	    $fix_com{$com_hash}{$tag_tail} = 1;
	    print '.';
	}
    }
    close $git_tag
	or warn $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git tag";
    print "\n";
    
    my $num_com = keys(%fix_com);
    my $current_com = 0;
    my $count = 0;
    
    my $pm;
    if ($self->{parallel} > 1) {
	require Parallel::ForkManager;
	$pm = Parallel::ForkManager->new($self->{parallel});
	print STDERR "[INFO] Parallel execution: # of forked processes=",$self->{parallel},"\n";
    }

    for my $commit (keys %fix_com) {
	$current_com++;

	if ($self->{parallel} > 1) {
	    $pm->start and next;
	}

	print "($current_com/$num_com)\n";
	
	print "--->SZZ step2: diff";
	git_checkout($git_path, $commit);
	print "\n";
	
	my $git_diff_option = '-M -l 3000 --name-status';
	my %file_lines = ();
	
	open my $git_diff, "-|", "git --git-dir=$git_path/.git diff $git_diff_option"
	    . " HEAD^ HEAD"
	    or die "Couldn't open git diff: $!";
	while (my $line = <$git_diff>) {
	    next if ($line =~ /\.list$/);
	    next if ($line =~ /^A/ or $line =~ /^D/ or $line =~ /^C/ or $line =~ /^R100/);
	    
	    if ($line =~ /^R\d+\s+(\S+)\s+(\S+)\n$/) {
		my $pre_file = $1;
		my $post_file = $2;
		szz_diff_simple(\%file_lines, $git_path, $pre_file, $post_file);
	    }
	    elsif ($line =~ /^M\s+(\S+)\n$/) {
		my $file = $1;
		szz_diff_simple(\%file_lines, $git_path, $file);
	    }
	}
	close $git_diff
	    or die $! ? "Syserr closing pipe: $!" : "Wait status ". $? . " from git diff";
	
	if (keys %file_lines == 0) {
	    for my $tail1 (keys %{ $fix_com{$commit} }) {
		my $ftag = $tag_ini.'-'.$tail1;
		git_dtag($git_path, $ftag);
	    }
	}
	else {
	    my $ftag;
	    for my $tail2 (keys %{ $fix_com{$commit} }) {
		$ftag = $tag_ini.'-'.$tail2;
		szz_ftag(\%file_lines, $git_path, $ftag);
	    }
	    
	    print "--->SZZ step3: blame";
	    git_checkout($git_path, 'HEAD^');
	    my $ref_com_data = szz_blame_simple(\%file_lines, $git_path);
	    
	    for my $tail3 (keys %{ $fix_com{$commit} }) {
		$ftag = $tag_ini.'-'.$tail3;
		$tail3 =~ /^(.+)-\d+$/;
		my $id = $1;
		
		#print STDERR $id,":",$id2date{$id}{open},"\n";
		
		my ($yyyy, $mm, $dd) = ($id2date{$id}{open} =~ /(\d+)-(\d+)-(\d+)/);
		my $ep_id_open = timelocal(0, 0, 0, $dd, $mm-1, $yyyy-1900);
		my $num_bug_com = 0;
		
		for my $com (keys %$ref_com_data) {
		    my $ep_com_date = $ref_com_data->{$com}{date};
		    
		    if ($ep_com_date <= $ep_id_open) {
			$count++;
			$num_bug_com++;
			
			my $bug_tag = $ftag;
			$bug_tag =~ s/$tag_ini/$set_ini/;
			$bug_tag .= '-' . $count;
			
			szz_btag($ref_com_data->{$com}{file}, $git_path, $bug_tag, $com);
			print " [Tagged ($bug_tag)]\n";
		    }
		}
		
		if ($num_bug_com == 0) {
		    git_dtag($git_path, $ftag);
		}
	    }
	}
	if ($self->{parallel} > 1) {
	    $pm->finish;
	}
    }
    if ($self->{parallel} > 1) {
	$pm->wait_all_children;
    }
    print "\n";
    
    git_checkout($git_path, $now_head);
    git_gc($git_path);
}

sub setFAULTtag {
    my ($self) = @_;

    my $TARGET = $self->{git_path};
    my $QUIET;
    my $TAG   = 1;

    my $tag_ini = $self->{tag_ini};
    my $set_ini = $self->{set_ini};
    my $fault_ini = $self->{fault_ini};
    my $innocent_ini = $self->{innocent_ini};

    print STDERR "[INFO] Getting bug tags\n" if (!$QUIET);
    my @list_BUGtag = split(/\n/,`git --git-dir=$TARGET/.git tag -l "$set_ini-*"`);
    
    my %found;

    print STDERR "[INFO] Searching lready tagged as $fault_ini\n" if (!$QUIET);
    my @faulttags = ();
    @faulttags = split(/\n/,`git --git-dir=$TARGET/.git tag -l "$fault_ini-*"`);
    foreach (@faulttags) {
	$_=~/^${fault_ini}\-.+\-([0-9a-f]+)$/;
	$found{$1} = 1;
    }

    print STDERR "[INFO] Identifying faulty modules\n" if (!$QUIET);
    foreach my $bugtag (sort(@list_BUGtag)) {
	$bugtag =~/^${set_ini}-(\d+)-(\d+)-(\d+)$/;
	my $issue = $1;
	my $fixid = $2;
	my $bugid = $3;
	my $fixtag = "$tag_ini-$issue-$fixid";
	my @javabugfiles;

	# get buggy files at the point of bugtag
	my @bugfiles = split(/@@/,`git --git-dir=$TARGET/.git show --pretty=short $bugtag| head -4 | tail -1`);
	foreach my $f (@bugfiles) {
	    next if ($f =~/^\s*$/);

	    # if the target is not a histraged repository, 
            #    all we need are *.java files.
	    if (!$self->{historage}) {
		next if ($f !~/\.java$/);
	    # if the target is a historaged repository, 
            #    all we need are CL(class), MT(method), FE(field), CN(constructor)
	    } else {
		if ($f !~/\/(IN|MT|FE|CN|CL|CM)\// && $f !~/\.java$/) {
		    next;
		}
	    }
	    push(@javabugfiles,$f);
	}


	for (my $j = 0; $j <= $#javabugfiles; $j++){
	    my $jf = $javabugfiles[$j];

	    # List commits between bugtag and fixtag
	    my @commits = split(/\n/, `git --git-dir=$TARGET/.git log $bugtag..$fixtag -- '$jf' | grep "^commit " | cut -d ' ' -f 2`);
	    for (my $i = 0; $i<=$#commits;$i++) {
		my $c = $commits[$i];

		printf STDERR "[T:%20s C:%4d/%4d F:%4d/%4d] ",$bugtag,$i,$#commits,$j,$#javabugfiles;

		my $gitshowfile = `git --git-dir=$TARGET/.git show $c -- '$jf' | grep "^index "`;
		if ($gitshowfile eq "") {
		    print STDERR "\n";
		    next;
		}
		my ($dummy1,$hashes) = split(/\s+/,$gitshowfile);
		my $blobhash;
		my $dummy2;
		if ($i == 0) {
		    ($dummy2,$blobhash) = split(/\.\./,$hashes);
		} else {
		    ($blobhash,$dummy2) = split(/\.\./,$hashes);
		}
		chomp($blobhash);
		if ($blobhash =~/^0+$/ || $found{$blobhash}) {
		    print STDERR "\n";
		    next;
		}		    

		my $ftype = "unknown";
		if ($jf=~/\.java$/) {
		    $ftype = "java";
		} elsif ($jf=~/\/(MT|FE|CN|CM)\//) {
		    $ftype = $1;
		} elsif ($jf=~/\/(CL|IN)\//) {
		    $ftype = $1;
		} else {
		    print STDERR "\n[INFO] unknown $jf\n";
		}
		
		$found{$blobhash} = 1;
		printf STDERR "$ftype $blobhash\n";
		if ($TAG) {
		    system "git --git-dir=$TARGET/.git tag $fault_ini-$issue-$fixid-$bugid-$ftype-$blobhash $blobhash";
		}
	    }
	}
    }
    
    my @innocenttags = ();
    @innocenttags = split(/\n/,`git --git-dir=$TARGET/.git tag -l "$innocent_ini-*"`);
    foreach (@innocenttags) {
	$_=~/^${innocent_ini}\-.+\-([0-9a-f]+)$/;
	$found{$1} = 2;
    }

    print STDERR "[INFO] Identifying non-faulty modules\n" if (!$QUIET);
    my @faulthash = keys %found;
    my @allcommits = split(/\n/, `git --git-dir=$TARGET/.git log | grep "^commit "| cut -d ' ' -f 2`);
    
    for (my $i = 0; $i<=$#allcommits;$i++) {
	my $c = $allcommits[$i];
	my @files = ();
	@files = split(/\n/,`git --git-dir=$TARGET/.git show $c | grep "^index "`);
	my @filenames = ();
	@filenames = split(/\n/,`git --git-dir=$TARGET/.git show $c | grep "^diff --git"`);
	next if (!@files);
	for (my $j = 0; $j <= $#files; $j++) {
	    my ($dummy1,$hashes) = split(/\s+/,$files[$j]);
	    my $blobhash;
	    my $dummy2;
	    ($dummy2,$blobhash) = split(/\.\./,$hashes);
	    chomp($blobhash);
	    next if ($blobhash =~/^0+$/);
	    next if ($found{$blobhash});

	    my ($_diff,$_git,$_a,$jf) = split(/\s+/,$filenames[$j]);

	    my $ftype = "unknown";
	    if ($jf=~/\.java$/) {
		$ftype = "java";
	    } elsif ($jf=~/\/(MT|FE|CN|CM)\//) {
		$ftype = $1;
	    } elsif ($jf=~/\/(CL|IN)\//) {
		$ftype = $1;
	    } else {
		print STDERR "[INFO] unknown $jf\n";
	    }
	    
	    $found{$blobhash} = 2;
	    printf STDERR "[C:%4d/%4d F:%4d/%4d] $ftype $blobhash\n",$i,$#allcommits,$j,$#files if (!$QUIET);
	    if ($TAG) {
		system "git --git-dir=$TARGET/.git tag $innocent_ini-$ftype-$blobhash $blobhash";
	    }
	}
    }
}


1;
